package com.primeiro.homeworksimplegame

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    enum class Direction {
        FORWARD, BACKWARD, LEFT, RIGHT
    }

    private var gameIsRunning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonPlay.setOnClickListener {
            processPlayStopButton()
        }

        buttonForward.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                processAction(event, Direction.FORWARD)
                return v?.onTouchEvent(event) ?: true
            }
        })
        buttonBackward.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                processAction(event, Direction.BACKWARD)
                return v?.onTouchEvent(event) ?: true
            }
        })

        buttonLeft.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                processAction(event,Direction.LEFT)
                return v?.onTouchEvent(event) ?: true
            }
        })

        buttonRight.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                processAction(event, Direction.RIGHT)
                return v?.onTouchEvent(event) ?: true
            }
        })

    }

    // process play/stop button actions
    private fun processPlayStopButton(){
        if(mycanvas.gameIsOver) {
            buttonPlay.text = "play"
        }
        if(gameIsRunning == false){
            gameIsRunning = true
            mycanvas.startGame()
            buttonPlay.text = "stop"

        } else {
            gameIsRunning = false
            mycanvas.stopGame()
            buttonPlay.text = "play"
        }
    }

    // Process buttons touch action
    private fun processAction(event: MotionEvent?, direct: Direction){
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                when (direct) {
                    Direction.FORWARD -> mycanvas.goForward()
                    Direction.BACKWARD -> mycanvas.goBackward()
                    Direction.LEFT -> mycanvas.goLeft()
                    Direction.RIGHT -> mycanvas.goRight()
                }
            }
            MotionEvent.ACTION_UP -> {
                mycanvas.resetAction()
            }
        }
    }
}
