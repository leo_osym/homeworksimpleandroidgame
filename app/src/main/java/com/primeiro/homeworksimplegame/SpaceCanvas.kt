package com.primeiro.homeworksimplegame

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import stanford.androidlib.graphics.*


class SpaceCanvas(context: Context, attrs: AttributeSet): GCanvas(context, attrs)
{
    companion object {
        private const val FRAMES_PER_SECOND = 30
        private const val FORWARD_VELOCITY = 12f
        private const val SIDE_BACKWARD_VELOCITY = 10f
        private const val FORWARD_ACCELERATION = 0.5f
        private const val SIDE_BACKWARD_ACCELERATION = 0.5f
        private const val COLLISION_MARGIN = 100f
    }

    private lateinit var rocket: GSprite
    private lateinit var kaboomSprite: GSprite
    private lateinit var label: GLabel
    private var meteors = ArrayList<GSprite>()
    private var results = ArrayList<Int>()
    private var numberOfTicks = 0
    private var meteorImages = arrayOf(R.drawable.meteors_001, R.drawable.meteors_002,
        R.drawable.meteors_003, R.drawable.meteors_004)
    var gameIsOver = false
    private var score = 0

    override fun init() {
        backgroundColor = GColor.makeColor(0,165,172)
        var rocketImage = BitmapFactory.decodeResource(resources, R.drawable.space_ship)
        rocketImage = rocketImage.scaleToHeight(200f)
        // sprite of rocket
        rocket = GSprite(rocketImage, this.width/2f-rocketImage.width/2f,this.height-800f)
        add(rocket)
        // sprite of collision
        var kaboom = BitmapFactory.decodeResource(resources, R.drawable.collision).scaleToHeight(350f)
        kaboomSprite =  GSprite(kaboom, rocket.x, rocket.y)
        // set label with result and put to the canvas
        var paint = Paint()
        paint.color = Color.BLACK
        paint.typeface = Typeface.DEFAULT_BOLD
        paint.textSize = 100f
        label = GLabel(this, "Score: ${score}",0f, 0f)
        label.paint = paint
        label.sendForward()
    }

    fun startGame(){
        // clear everything and restart
        if(gameIsOver){
            gameIsOver = false
            backgroundColor = GColor.makeColor(0,165,172)
            //clear sprites
            for(meteor in meteors){
                remove(meteor)
            }
            meteors.clear()
            results.clear()
            remove(kaboomSprite)
            remove(kaboomSprite)
            remove(rocket)
            score = 0
            label.text = "Score: ${score}"
            rocket.x = this.width/2f-rocket.width/2f
            rocket.y = this.height*0.6f
            add(rocket)
        }
        animate(FRAMES_PER_SECOND){
            stopIfHitBounds()
            tick()
        }
    }

    private fun tick(){
        numberOfTicks ++
        rocket.update()
        checkCollision()
        if(numberOfTicks%60 == 1){
            addMeteor()
        }
        countScore()
    }

    private fun countScore(){
        for(i in (0..meteors.size-1)){
            meteors[i].update()
            if(meteors[i].y >= this.height){
                results[i] = 1
                remove(meteors[i])
            }
        }
        score = results.filter { s -> s == 1 }.size

        if(gameIsOver) {
            label.text = "Game Over! Score: ${score}"
        }
        else {
            label.text = "Score: ${score}"
        }
    }

    // add meteor with random speed, position and size
    private fun addMeteor(){
        // random meteor image
        var newMeteor = BitmapFactory.decodeResource(resources, meteorImages.random())
        var meteorSize = (200..400).random().toFloat()
        newMeteor = newMeteor.scale(meteorSize, meteorSize)
        // meteor sprite
        var randomMeteorPosition = (0..this.width-newMeteor.width).random().toFloat()
        var meteorSprite = GSprite(newMeteor, randomMeteorPosition, 0f)
        meteorSprite.velocityY = (5..15).random().toFloat()
        meteors.add(meteorSprite)
        results.add(0)
        add(meteorSprite)
    }

    // check if meteor collides with rocket
    private fun checkCollision(){
        for(meteor in meteors){

            meteor.collisionMarginBottom = COLLISION_MARGIN
            meteor.collisionMarginTop = COLLISION_MARGIN
            meteor.collisionMarginLeft = COLLISION_MARGIN
            meteor.collisionMarginRight = COLLISION_MARGIN

            if(meteor.collidesWith(rocket)){
                animationStop()
                gameIsOver = true
                // add colision sprite above the rocket position
                kaboomSprite.x = rocket.x
                kaboomSprite.y = rocket.y
                add(kaboomSprite)
            }
        }
    }

    fun stopGame(){
        animationStop()
    }

    fun goLeft(){
        resetAction()
        rocket.velocityX = -SIDE_BACKWARD_VELOCITY
        rocket.accelerationX = -SIDE_BACKWARD_ACCELERATION
    }

    fun goRight(){
        resetAction()
        rocket.velocityX = SIDE_BACKWARD_VELOCITY
        rocket.accelerationX = SIDE_BACKWARD_ACCELERATION
    }

    fun goForward(){
        resetAction()
        rocket.velocityY = -FORWARD_ACCELERATION
        rocket.accelerationY = -FORWARD_VELOCITY
    }

    fun goBackward(){
        resetAction()
        rocket.velocityY = SIDE_BACKWARD_VELOCITY
        rocket.accelerationY = SIDE_BACKWARD_ACCELERATION
    }

    fun resetAction(){
        rocket.velocityX = 0f
        rocket.accelerationX = 0f
        rocket.velocityY = 0f
        rocket.accelerationY = 0f
    }

    fun stopIfHitBounds(){
        if(rocket.x < 0f) {
            rocket.x = 0f
            resetAction()
        }
        if(rocket.y < 0f){
            rocket.y = 0f
            resetAction()
        }
        if((rocket.x + rocket.width) > this.width){
            rocket.x = this.width - rocket.width
            resetAction()
        }
        if((rocket.y + rocket.height) > this.height-600f){
            rocket.y = this.height-600f - rocket.height
            resetAction()
        }
    }
}